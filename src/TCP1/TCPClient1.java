/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TCP1;

import java.io.*;
import static java.lang.Thread.sleep;
import java.net.*;
import java.util.Scanner;

class TCPClient1 {

    public static String sentence;
    public String modifiedSentence;
    public static int msgNumber = 0;
    public static int clientNumber = 0;

    public static void main(String argv[]) throws Exception {

        Scanner in = new Scanner(System.in);
        Socket clientSocket = null;
        String msg = "";
        System.out.println("Informe o id do cliente:");
        String cliente = in.nextLine();
        while ( !msg.equals("*fim")) {
            msg = in.nextLine();
            clientSocket = new Socket("localhost", 6789);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            outToServer.writeBytes("Cliente " + cliente + ": " +  msg + '\n');
            sleep(1);
            clientSocket.close();
        }
        
    }
}
