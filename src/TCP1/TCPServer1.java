/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TCP1;


import java.io.*;
import java.net.*;

public class TCPServer1 {

    public static void main(String argv[]) throws Exception {

        String clientSentence = "";
        BufferedReader inFromClient;
        ServerSocket welcomeSocket = new ServerSocket(6789);
        Socket connectionSocket;
        
        while (!clientSentence.equals("*fim")) {
            connectionSocket = welcomeSocket.accept();
            if (connectionSocket != null) {
                inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                clientSentence = inFromClient.readLine();
                System.out.printf("%s\n", clientSentence);
                connectionSocket.close();
            }
        }
        welcomeSocket.close();
    }
}
