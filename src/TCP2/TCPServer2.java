/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP2;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class TCPServer2 {

    public static void main(String argv[]) throws Exception {
        ServerSocket welcomeSocket = new ServerSocket(6789);
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            if (connectionSocket != null) {
                Server2 client = new Server2(connectionSocket);
                client.start();
            }
        }
    }
}

class Server2 extends Thread {

    private Socket connectionSocket;
    private String clientSentence;
    private String capitalizedSentence;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;
    private int clientid;

    public Server2(Socket c) throws IOException {
        connectionSocket = c;
    }

    public void run() {
        while (true) {
            try {
                inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
                clientSentence = inFromClient.readLine();

                Date date = Calendar.getInstance().getTime();
                String data = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(date);

                System.out.printf("%s (às %s)\n", clientSentence, data);

            } catch (IOException e) {
                System.out.println("Errore: " + e);
            }
        }
    }
}
