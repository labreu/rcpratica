/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TCP3;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

class TCPServer3 {

    public static void main(String argv[]) throws Exception {
        ServerSocket welcomeSocket = new ServerSocket(6789);
        while (true) {
            Socket connectionSocket = welcomeSocket.accept();
            if (connectionSocket != null) {
                Server3 client = new Server3(connectionSocket);
                client.start();
            }
        }
    }
}

class Server3 extends Thread {

    private Socket connectionSocket;
    private String clientSentence;
    private String capitalizedSentence;
    private BufferedReader inFromClient;
    private DataOutputStream outToClient;
    private int clientid;

    public Server3(Socket c) throws IOException {
        connectionSocket = c;
    }

    public void run() {
        while (true) {
            try {
                inFromClient = new BufferedReader(new InputStreamReader(connectionSocket.getInputStream()));
//            System.out.println(connectionSocket);
                clientSentence = inFromClient.readLine();

                String clientTimeString = clientSentence.substring(clientSentence.length() - 14, clientSentence.length());

                clientSentence = clientSentence.substring(0, clientSentence.length() - 14);
                Date date = Calendar.getInstance().getTime();
                String data = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(date);

                long clientTimeLong = Long.parseLong(clientTimeString);

                long delay = System.nanoTime() - clientTimeLong;
                System.out.printf("%s (às %s com delay de %dns)\n", clientSentence, data, delay);
            } catch (IOException e) {
                System.out.println("Errore: " + e);
            }
        }
    }
}
